package com.zuitt.wdc044.models;

import javax.persistence.*;

// Marks this Java Object as a representation of a database table via @Entity
@Entity

@Table( name = "posts" )
public class Post {
    // Indicates that this property represent the primary key
    @Id
    // Values for this property will be auto-incremented
    @GeneratedValue
    private Long id;
    // Class properties that represent table columns in a relational database and is annotated by Column
    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;



    // Constructor
    public Post(){}

    public Post(String title, String content, User user){
        this.title = title;
        this.content = content;
        this.user = user;
    }



    // Getters and Setters
    public String getTitle(){
        return this.title;
    }

    public String getContent(){
        return this.content;
    }

    public User getUser() {
        return user;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setContent(String content){
        this.content = content;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
